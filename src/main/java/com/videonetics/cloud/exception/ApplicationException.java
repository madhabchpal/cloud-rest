package com.videonetics.cloud.exception;

import org.springframework.http.HttpStatus;

public class ApplicationException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private int code;
	
	private HttpStatus status;
	
	public ApplicationException() {
		super();
    }
	
	public ApplicationException(String message) {
		super(message);
    }
	
	public ApplicationException(int code, HttpStatus status, String message) {
		super(message);
		this.code = code;
		this.status = status;
    }

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public HttpStatus getStatus() {
		return status;
	}
	
	public int getStatusCode() {
		return status.value();
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
}
