package com.videonetics.cloud.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.videonetics.cloud.vo.ResponseVO;

@Component
@ControllerAdvice(basePackages={"com.videonetics.cloud.rest.controller"})
public class RestControllerAdvice {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(RestControllerAdvice.class);
	
	@ExceptionHandler(UnknownResourceException.class)
	protected ResponseEntity<ResponseVO> handleUnknownResourceException(HttpServletRequest request, HttpServletResponse response,
			UnknownResourceException ure) {
		LOGGER.error("UnknownResourceException raised at {}", request.getRequestURL(), ure);

		ResponseVO restResponse = new ResponseVO(ure, request.getRequestURI());
		return new ResponseEntity<ResponseVO>(restResponse, ure.getStatus());
	}
	
	@ExceptionHandler(ApplicationException.class)
	protected ResponseEntity<ResponseVO> handleApplicationException(HttpServletRequest request, HttpServletResponse response,
			ApplicationException ae) {
		LOGGER.error("ApplicationException raised at {}", request.getRequestURL(), ae);

		ResponseVO restResponse = new ResponseVO(ae, request.getRequestURI());
		return new ResponseEntity<ResponseVO>(restResponse, ae.getStatus());
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	protected ResponseEntity<ResponseVO> handleIllegalArgumentException(HttpServletRequest request, HttpServletResponse response,
			IllegalArgumentException iae) {
		LOGGER.error("IllegalArgumentException raised at {}", request.getRequestURL(), iae);
		
		ResponseVO restResponse = new ResponseVO(VReturnValues.VA_ERROR, HttpStatus.BAD_REQUEST.value(), iae.getMessage(), request.getRequestURI());
		return new ResponseEntity<ResponseVO>(restResponse, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(NullPointerException.class)
	protected ResponseEntity<ResponseVO> handleNullPointerException(HttpServletRequest request, HttpServletResponse response,
			NullPointerException npe) {
		LOGGER.error("NullPointerException raised at {}", request.getRequestURL(), npe);
		
		ResponseVO restResponse = new ResponseVO(VReturnValues.VA_ERROR, HttpStatus.BAD_REQUEST.value(), npe.getMessage(), request.getRequestURI());
		return new ResponseEntity<ResponseVO>(restResponse, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(RuntimeException.class)
	protected ResponseEntity<ResponseVO> handleRuntimeException(HttpServletRequest request, HttpServletResponse response,
			RuntimeException re) {
		LOGGER.error("RuntimeException raised at {}", request.getRequestURL(), re);
		
		ResponseVO restResponse = new ResponseVO(VReturnValues.VA_ERROR, HttpStatus.BAD_REQUEST.value(), re, request.getRequestURI());
		return new ResponseEntity<ResponseVO>(restResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(Exception.class)
	protected @ResponseBody ResponseEntity<Object> handleException(HttpServletRequest request, HttpServletResponse response, Exception e) {
		LOGGER.error("Exception raised at {}", request.getRequestURL(), e);

		ResponseVO restResponse = new ResponseVO(VReturnValues.VA_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.value(), e, request.getRequestURI());
		return new ResponseEntity<Object>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
