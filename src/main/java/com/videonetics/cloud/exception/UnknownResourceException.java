package com.videonetics.cloud.exception;

import org.springframework.http.HttpStatus;


public class UnknownResourceException extends RuntimeException {

	private static final long serialVersionUID = 2L;
	
	private int code;
	
	private HttpStatus status;
	
	public UnknownResourceException() {
		super();
    }
	
	public UnknownResourceException(String message) {
		super(message);
    }
	
	public UnknownResourceException(int code, HttpStatus status, String message) {
		super(message);
		this.code = code;
		this.status = status;
    }

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public HttpStatus getStatus() {
		return status;
	}
	
	public int getStatusCode() {
		return status.value();
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
}
