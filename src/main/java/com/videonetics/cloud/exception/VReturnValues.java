package com.videonetics.cloud.exception;

public interface VReturnValues {

	// GLOBAL
	static final int SUCCESS = 0;

	// Va engine related
	static final int VA_WARNING = 1;
	static final int VA_ERROR = 2;
	static final int VA_NULLPOINTER = 3;
	static final int VA_SIZEMISMATCH = 4;
	static final int VA_PARSINGFAILED = 5;
	static final int VA_WRONGFORMAT = 6;
	static final int VA_OUTOFBOUND = 7;
	static final int VA_INVALIDVALUE = 8;
	static final int VA_LOWBUFFERSIZE = 9;
	static final int VA_DATAYETNOTPREPARED = 10;
	static final int VA_LIBRARYYETNOTINITIALIZED = 11;
	static final int VA_LIBRARYALREADYINITIALIZED = 12;
	static final int VA_IMPROPERCALL = 13;
	static final int VA_VA_DUPLICATEID = 14;
	static final int VA_INVALIDID = 15;
	static final int VA_VALUENOTASSIGNED = 16;
	static final int VA_IGNORE = 19;

	static final int VA_PARAMETERNOTEXIST = 30;
	static final int VA_PARAMETEROUTOFBOUND = 31;
	static final int VA_PARAMETERNOTEDITABLE = 32;
	static final int VA_INVALIDPARAMETERVALUE = 33;
	static final int VA_WRONGPARAMETERSTRINGFORMAT = 34;
	static final int VA_MANDATORYPARAMETERSYETNOTSET = 35;
	static final int VA_INSUFFICIENTPARAMETERS = 36;
	static final int VA_NULLPARAMETER = 37;
	static final int VA_PARAMETERNOTSENT = 38;
	static final int VA_PARAMETERRECEIVEDSUCCESSFULLY = 39;
	static final int VA_PARAMETERSPARTIALLYACCEPTED = 40;
	static final int VA_INCOMPATIBLEPARAMETERS = 41;
	static final int VA_PARAMETERMULTIPLEDEFINITION = 42;

	static final int VA_INVALIDCOORDINATES = 55;
	static final int VA_INVALIDOPERATION = 56;
	static final int VA_INVALIDGRAPHICSTYPE = 57;
	static final int VA_TOOMANYOBJECTS = 58;
	static final int VA_INVALIDTOTALCONTROLPARAMETERS = 59;

	static final int VA_MEMORYOVERFLOW = 70;

	static final int VA_CHANNELNOTEXIST = 80;
	static final int VA_DUPLICATECHANNEL = 81;
	static final int VA_CHANNELOVERFLOW = 82;

	static final int VA_TRYAGAIN = 100;
	static final int VA_WAIT = 101;
	static final int VA_BUSY = 102;

	static final int VA_EVALUATION_PERIOD_OVER = 150;
	static final int VA_CONTINUOUS_EVALUATION_OVER = 151;
	static final int VA_SYSTEM_FILE_CORRUPTED = 152;
	static final int VA_INVALID_HARDWARE = 153;
	static final int VA_VALIDITY_EXPIRED = 154;
	static final int VA_SYS_DATE_ELAPSED = 155;
	static final int VA_BEYOND_INS = 156;
	static final int VA_NO_AVAI_LINS = 157;
	static final int VA_LICENSE_CHECK_FAILED = 158;

	static final int CALL_NOT_IMPLEMENTED = 999;

	// Database related
	static final int DB_CONNECTION_ERROR = 1000;
	static final int DB_STATEMENT_ERROR = 1001;
	static final int DB_UNKNOWN_ERROR = 1002;
	static final int DB_DUPLICATE_KEY_ERROR = 1003;
	static final int DB_WRITE_ERROR = 1004;

	static final int DUPLICATE_MEDIA_SERVER_ID = 1050;
	static final int INVALID_MEDIA_SERVER_ID = 1051;
	static final int CHANNEL_NOT_AVAILABLE = 1052;
	static final int CHANNEL_RELATION_NOT_AVAILABLE = 1053;
	static final int MEDIASERVER_NOT_AVAILABLE = 1054;
	static final int SERVER_INTERFACE_NOT_AVAILABLE = 1055;
	static final int STORAGE_COMPONENT_NOT_AVAILABLE = 1056;
	static final int NO_MEDIA_SERVER_AVAILABLE = 1057;
	static final int INVALID_ANALYTIC_SERVER_ID = 1058;
	static final int PROXY_VMS_SERVER_NOT_AVAILABLE = 1059;

	// USER RELATED
	static final int USER_ALREADY_LOGED_IN = 1100;
	static final int USER_INVALID_USERNAME_PASSWORD = 1101;
	static final int USER_INVALID_SESSION = 1102;
	static final int USER_PERMISSION_DENIED = 1103;
	static final int USER_DETAILS_NOT_AVAILABLE = 1104;
	static final int USER_INVALID_PROFILE_AUTHENTICATION = 1105;
	static final int USER_PENDING_MESSAGE = 1106;
	static final int USER_INVALID_CLIENT_MACHINE = 1107;

	// PTZ CONTROL
	static final int PTZ_TOO_MANY_REQUEST = 1200;
	static final int PTZ_SESSION_OCCUPIED = 1201;
	static final int CAMERA_DRIVER_NOT_AVAILABLE = 1202;
	static final int CAMERA_PARAMETER_NOT_SUPPORTED = 1203;
	static final int CHANNEL_NUMBER_EXIST = 1204;
	// channel

	// Recording details
	static final int RECORDING_PROFILE_NOT_AVAILABLE = 1400;

	static final int STORAGE_SERCHING_NOT_COMPLETED = 1450;
	static final int STORAGE_NOT_AVAILABLE = 1451;
	static final int STORAGE_LOW = 1452;

	static final int SSC_NULL_OBJECT_REQUEST = 1500;
	static final int SSC_ARGUMENT_MISMATCH = 1501;
	static final int SSC_TRUNCATED_OBJ_BYTES = 1502;
	static final int SSC_INVALID_REQUEST_ID = 1503;
	static final int SSC_CONNECTION_ERROR = 1504;
	static final int SSC_SENDING_REQUEST_ERROR = 1505;
	static final int SSC_ACCESS_RESPONSE_ERROR = 1506;
	static final int SSC_SERIALIZABLE_ERROR = 1507;
	static final int SSC_FILE_NOT_FOUND = 1508;
	static final int SSC_FILE_SIZE_OUT_OF_BOUND = 1509;
	static final int VA_CONNECTION_ERROR = 1510;
	static final int VA_REQUEST_ERROR = 1511;
	static final int VA_RESPONSE_ERROR = 1512;
	static final int VA_WRAPPER_NOT_AVAILABLE = 1513;

	static final int RLVD_CONNECTION_ERROR = 1514;
	static final int RLVD_REQUEST_ERROR = 1515;
	static final int RLVD_RESPONSE_ERROR = 1516;
	static final int RLVD_WRAPPER_NOT_AVAILABLE = 1517;

	static final int STREAMING_INVALID_REQUEST = 1550;
	static final int STREAMING_INVALID_SESSION = 1551;
	static final int STREAMING_INVALID_REQUEST_FORMAT = 1552;

	static final int RECORD_NOT_AVAILABLE = 1600;
	static final int MATRIX_SIZE_MISMATCH = 1601;

	static final int LICENSE_CHANNEL_OVER = 1700;
	static final int LICENSE_USER_OVER = 1701;
	static final int LICENSE_TIME_OVER = 1702;
	static final int WATCHDOG_CONNECTION_ERROR = 1703;
	static final int WATCHDOG_RESPONSE_ERROR = 1704;
}
