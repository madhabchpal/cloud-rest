package com.videonetics.cloud.vo;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.videonetics.cloud.exception.ApplicationException;
import com.videonetics.cloud.exception.UnknownResourceException;

public class ResponseVO {

	private int code;

	private int status;

	private String message;

	private String uri;

	private List<Object> result = new ArrayList<Object>();
	
	public ResponseVO(UnknownResourceException ure, String uri) {
		this.code = ure.getCode();
		this.status = ure.getStatusCode();
		this.message = ure.getMessage();
		this.uri = uri;
	}
	
	public ResponseVO(ApplicationException ae, String uri) {
		this.code = ae.getCode();
		this.status = ae.getStatusCode();
		this.message = ae.getMessage();
		this.uri = uri;
	}
	
	public ResponseVO(int code, int status, RuntimeException re, String uri) {
		this.code = code;
		this.status = status;
		this.message = re.getMessage();
		this.uri = uri;
	}
	
	public ResponseVO(int code, int status, Exception e, String uri) {
		this.code = code;
		this.status = status;
		this.message = e.getMessage();
		this.uri = uri;
	}

	public ResponseVO(int code, int status, String message) {
		this.code = code;
		this.status = status;
		this.message = message;
	}

	public ResponseVO(int code, int status, String message, String uri) {
		this.code = code;
		this.status = status;
		this.message = message;
		this.uri = uri;
	}
	
	@SuppressWarnings("unchecked")
	public ResponseVO(int code, int status, String message, String uri, Object object) {
		this.code = code;
		this.status = status;
		this.message = message;
		this.uri = uri;
		if(object != null) {
			if(object instanceof List) {
				this.result = (List<Object>) object;
			} else {
				this.result.add(object);
			}
		}
	}

	public int getCode() {
		return code;
	}

	public int getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public String getUri() {
		return uri;
	}

	public List<Object> getResult() {
		return result;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
