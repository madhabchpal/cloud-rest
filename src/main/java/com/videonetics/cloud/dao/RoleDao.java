package com.videonetics.cloud.dao;

import java.util.List;

import com.videonetics.cloud.mongo.collections.VRole;

public interface RoleDao {

	List<VRole> getRoles();

}
