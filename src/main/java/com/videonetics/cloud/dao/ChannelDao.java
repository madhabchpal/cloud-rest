package com.videonetics.cloud.dao;

import java.util.List;

import com.videonetics.model.component.VMediaChannel;

public interface ChannelDao {

	List<VMediaChannel> getChannels();

	VMediaChannel getChannel(String channelId);

}
