package com.videonetics.cloud.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.support.TransactionTemplate;

import com.videonetics.cloud.dao.ChannelDao;
import com.videonetics.cloud.properties.VChannelTable;
import com.videonetics.model.component.VMediaChannel;

@Repository
public class ChannelDaoImpl implements ChannelDao {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ChannelDaoImpl.class);

	@Autowired
	private DataSourceTransactionManager transactionManager;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private TransactionTemplate transactionTemplate;

	@Override
	public List<VMediaChannel> getChannels() {
		LOGGER.debug("Fetching all channels");
		
				String statementString = "select * from " + VChannelTable.table_name 
						/*+ " , " + VRlChannelGroupTable.table_name 
						+ " ,"  + VRlChannelLocationTable.table_name
						+ " ,"  + VRlChannelRecordingTable.table_name
						+ " where " + VChannelTable.table_name +  "." + VChannelTable.id
						+ " = "  + VRlChannelGroupTable.table_name + "." + VRlChannelGroupTable.channelId
						+ " and " + VChannelTable.table_name +  "." + VChannelTable.id
						+ " = "  + VRlChannelLocationTable.table_name + "." + VRlChannelLocationTable.channelId
						+ " and " + VChannelTable.table_name +  "." + VChannelTable.id
						+ " = "  + VRlChannelRecordingTable.table_name + "." + VRlChannelRecordingTable.channelId*/
						+ " order by " + VChannelTable.table_name +  "." + VChannelTable.number + " ASC ";


				List<VMediaChannel> vChannelList = jdbcTemplate.query( statementString,new RowMapper<VMediaChannel>(){
					@Override
					public VMediaChannel mapRow(ResultSet rs, int rowNumber) throws SQLException 
					{
						VMediaChannel vChannel = new VMediaChannel();


						vChannel.setId(rs.getShort(VChannelTable.id));
						vChannel.setIp(rs.getString(VChannelTable.ip));
						vChannel.setCommandPort(rs.getShort(VChannelTable.commandPort));
						vChannel.setIsPTZ((byte) rs.getShort(VChannelTable.isPtz));
						vChannel.setModel(rs.getString(VChannelTable.model));
						vChannel.setName(rs.getString(VChannelTable.name));
						vChannel.setPassword(rs.getString(VChannelTable.password));
						vChannel.setRecordingStream((byte) rs.getShort(VChannelTable.recordingStream));
						vChannel.setNumber(rs.getShort(VChannelTable.number));

						return vChannel;
					}
				});

				
		

		return vChannelList;

	}

	@Override
	public VMediaChannel getChannel(String channelId) {
		LOGGER.debug("Fetching channelid >> {}", channelId);
		
		return null;
	}

}
