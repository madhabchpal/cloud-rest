package com.videonetics.cloud.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.videonetics.cloud.dao.RoleDao;
import com.videonetics.cloud.mongo.collections.VRole;

@Repository
public class RoleDaoImpl implements RoleDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(RoleDaoImpl.class);

	@Autowired
	private MongoTemplate mongoTemplate;

	@Value("${mongo.collection.vrole}")
	private String vRoleCollection;
	
	@Override
	public List<VRole> getRoles() {
		LOGGER.debug("Get all role");
		return mongoTemplate.findAll(VRole.class, vRoleCollection);
	}

}
