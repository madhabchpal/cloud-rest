package com.videonetics.cloud.properties;

public interface VLogTable {
	
	String id = "id";
	String level = "level";
	String module = "module";
	String component = "component";
	String status = "status";
	String message = "message";
	String logTimestamp = "log_timestamp";
	String vUser = "v_user";
	
	String table_name = "v_log";

}
