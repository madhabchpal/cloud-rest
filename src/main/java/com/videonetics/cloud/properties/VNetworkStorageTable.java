package com.videonetics.cloud.properties;
/**
 * 
 * @author Dhiraj
 *
 */
public interface VNetworkStorageTable {

	//TODO: Not following standard naming convention
	String TABLE_NAME = "V_NETWORK_STORAGE".toLowerCase();
	String ID = "ID";
	String TYPE = "TYPE";
	String PATH = "PATH";
	String USERNAME = "USERNAME";
	String PASSWORD = "PASSWORD";
	String CAPACITY_IN_GB = "CAPACITY_IN_GB";

}
