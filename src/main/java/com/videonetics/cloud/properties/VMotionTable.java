package com.videonetics.cloud.properties;

public interface VMotionTable {
	
	String id = "id";
	String channelId = "channel_id";
	String motionStarttime = "motion_starttime";
	String motionEndtime = "motion_endtime";
	String motionPercentage = "motion_percentage";
		
	
	String table_name = "v_motion";

}
