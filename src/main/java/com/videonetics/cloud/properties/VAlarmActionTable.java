package com.videonetics.cloud.properties;

public interface VAlarmActionTable {
	
	String id = "id";
	String listnerId = "listner_id";
	String tiggerId = "trigger_id";
	String priority = "priority";
	
	String table_name = "v_alarm_action";

}
