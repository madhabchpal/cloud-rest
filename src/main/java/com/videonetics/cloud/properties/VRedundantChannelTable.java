package com.videonetics.cloud.properties;

public interface VRedundantChannelTable {
	
	String id = "id";
	String channelId = "channel_id";
	
	String table_name = "v_redundant_channel";

}
