package com.videonetics.cloud.properties;

public interface VJunctionTable {
	
	String id = "id";
	String name = "name";
	String controllerIp = "controller_ip";
	String controllerPort = "controller_port";
	String noOfBits = "no_of_bits";
	String mobileNumbers1 = "contact_number1";
	String mobileNumbers2 = "contact_number2";
	String mobileNumbers3 = "contact_number3";
	String mobileNumbers4 = "contact_number4";
	String table_name = "v_junction";

}
