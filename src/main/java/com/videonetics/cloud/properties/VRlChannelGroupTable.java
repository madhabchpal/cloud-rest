package com.videonetics.cloud.properties;

public interface VRlChannelGroupTable {
	
	String channelId = "channel_id";
	String groupId = "group_id";
	
	String table_name = "rl_channel_group";

}
