package com.videonetics.cloud.properties;

public interface VRlChannelUserTable {
	
	String channelId = "channel_id";
	String profileId = "profile_id";
	
	String table_name = "rl_channel_user";

}
