package com.videonetics.cloud.properties;

public interface VStorageTableVMS {
	
	String id = "id";
	String path = "path";
	String type = "type";
	String upload = "upload";
	
	String table_name = "v_storage_vms";

}
