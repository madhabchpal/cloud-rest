package com.videonetics.cloud.properties;

public interface VRlChannelJunctionTable {
	
	String channelId = "channel_id";
	String junctionId = "junction_id";
	
	String table_name = "rl_channel_junction";

}
