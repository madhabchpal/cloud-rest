package com.videonetics.cloud.properties;

public interface VRLVDEventTable {
	
	String id = "id";
	String channelId = "channel_id";
	String objectID = "object_id";
	String modifiedObjectID = "modified_object_id";
	String rlvdTime = "rlvd_time";
	String dontDelete = "dont_delete";
	String challanStatus = "challan_status";
	String challanNumber = "challan_number";
	String challanGeneratedBy = "challan_generated_by";
	String location = "location";
	String snapUrl = "snap_url";
	String evidenceSnapUrl = "evidence_snap_url";
	String eventId = "event_id";
	String paid = "paid";
	String challanGenerationTime = "challan_generation_time";
	String challanUrl = "challan_url";
	
	String table_name = "v_rlvd_temp";

}
