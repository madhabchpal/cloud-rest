package com.videonetics.cloud.properties;

public interface VRecycleEventSnapTable {
	
	String id = "id";
	String snapUrl = "snap_url";
	String eventSnapId = "event_snap_id";
	
	String table_name = "v_recyle_event_snap";

}
