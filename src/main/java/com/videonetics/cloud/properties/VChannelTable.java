package com.videonetics.cloud.properties;

public interface VChannelTable {
	
	String id = "id";
	String model = "model";
	String ip = "ip";
	String commandPort = "command_port";
	String name = "name";
	String isPtz = "is_ptz";
	String snapUrl = "snap_url";
	String analyticUrl = "analytic_url";
	String minorUrl = "minor_url";
	String majorUrl = "major_url";
	String username = "username";
	String password = "password";
	String recordingStream = "recording_stream";
	//String number = "ch_number";
	String number = "number";
	String chType = "ch_type";
	String latitude = "latitude";
	String longitude = "longitude";
	//String table_name = "v_channel";
	String table_name = "v_media_channel";

}
