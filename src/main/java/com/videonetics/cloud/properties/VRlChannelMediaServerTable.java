package com.videonetics.cloud.properties;
/**
 * 
 * @author Dhiraj
 *
 */
public interface VRlChannelMediaServerTable {

	String channelId = "channel_id";
	String mediaServerId = "media_server_id";
	String engagedFrom = "engaged_from";
	String table_name = "rl_channel_media_server";
}
