package com.videonetics.cloud.properties;

import java.io.Serializable;

public class MysqlDbConnectionProperty implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mysqlDatabaseUrl;
	private String username;
	private String password;
	
	
	
	
	/**
	 * @return the mysqlDatabaseUrl
	 */
	public String getMysqlDatabaseUrl() {
		return mysqlDatabaseUrl;
	}
	/**
	 * @param mysqlDatabaseUrl the mysqlDatabaseUrl to set
	 */
	public void setMysqlDatabaseUrl(String mysqlDatabaseUrl) {
		this.mysqlDatabaseUrl = mysqlDatabaseUrl;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}
