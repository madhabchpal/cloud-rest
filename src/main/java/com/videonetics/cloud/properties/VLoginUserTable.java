package com.videonetics.cloud.properties;

public interface VLoginUserTable {
	
	String id = "id";
	String profileId = "profile_id";
	String name = "name";
	String password = "password";
	String mailId = "mail_id";
	String mobile = "mobile";
	String sequrityQuestion_1 = "sequrity_question_1";
	String sequrityAnswer_1 = "sequrity_answer_1";
	String sequrityQuestion_2 = "sequrity_question_2";
	String sequrityAnswer_2 = "sequrity_answer_2";
	String userLoginTimestamp = "login_timestamp";
	String userLogoutTimestamp = "logout_timestamp";
	String machineUser = "machine_user";
	String machineId = "machine_id";
	
	String table_name = "v_login_user";

}
