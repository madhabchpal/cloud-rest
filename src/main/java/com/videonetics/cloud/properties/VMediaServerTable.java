package com.videonetics.cloud.properties;
/**
 * 
 * @author Dhiraj
 *
 */
public interface VMediaServerTable {

	
	String id = "id";
	String name = "name";
	String ip = "ip";
	String capacity = "capacity";
	String lastUpdateTime = "last_update_time";
	String tableName = "v_media_server";
}
