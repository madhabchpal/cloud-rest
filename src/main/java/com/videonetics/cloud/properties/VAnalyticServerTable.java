package com.videonetics.cloud.properties;

public interface VAnalyticServerTable {
	
	String id = "id";
	String name = "name";
	String ip = "ip";
	String engine = "engine";
	
	String table_name = "v_analytic_server";

}
