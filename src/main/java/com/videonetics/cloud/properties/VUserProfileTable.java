package com.videonetics.cloud.properties;

public interface VUserProfileTable {

	String profileId = "profile_id";
	String profileName = "profile_name";
	String priority = "priority";
	String wsAccessPermission = "ws_access_permission";
	String storageManagement = "storage_management";
	String searchAddNewCamera = "search_add_new_camera";
	String setAnalytics = "set_analytics";
	String createNewSchedule = "create_new_schedule";
	String userManagement = "user_management";
	String configExistingCamera = "config_existing_camera";
	String smartSearch = "smart_search";
	String ptzControl = "ptz_control";
	String acknowledgeEvent = "acknowledge_event";
	
	String table_name = "v_user_profile";
	
}


