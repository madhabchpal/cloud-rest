package com.videonetics.cloud.properties;

public interface VMapTable {
	
	String id = "id";
	String ref_map_id = "ref_map_id";
	String name = "name";
	String posX = "pos_x";
	String posY = "pos_y";
	String jpegImage = "jpeg_image";
	
	String table_name = "v_map";

}
