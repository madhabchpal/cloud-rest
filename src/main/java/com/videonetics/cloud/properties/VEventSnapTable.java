package com.videonetics.cloud.properties;

public interface VEventSnapTable {
	
	String id = "id";
	String snapUrl = "snap_url";
	String eventId = "event_id";
	
	String table_name = "v_event_snap";

}
