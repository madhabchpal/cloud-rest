package com.videonetics.cloud.properties;

public interface VEventTable {
	
	String id = "id";
	String channelId = "channel_id";
	String eventType = "event_type";
	String eventMessage = "event_message";
	String action = "action";
	String objectId = "object_id";
	String modifiedObjectId = "modified_object_id";
	String clipReview = "clip_review";
	String acknowledge = "acknowledge";
	String acknowledgeUser = "acknowledge_user";
	String priority = "priority";
	String eventStartTime = "event_starttime";
	String eventEndTime = "event_endtime";
	String dontDelete = "dont_delete";
	String zoneId = "zone_id";
	String escalatedStatus = "escalated_status";
	String objectProperty1 = "object_property_1";
	String objectProperty2 = "object_property_2";
	String objectProperty3 = "object_property_3";
	String objectProperty4 = "object_property_4";
	String table_name = "v_event";

}
