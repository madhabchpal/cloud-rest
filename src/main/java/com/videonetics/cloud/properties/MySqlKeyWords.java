package com.videonetics.cloud.properties;
/**
 * 
 * @author Dhiraj
 *
 */
public interface MySqlKeyWords {

    String SELECT = " SELECT ";
	String SELECT_STAR = " SELECT * ";
	String COUNT_STAR  = " COUNT(*)";
	String AS     = " AS ";
	String AND = " AND ";
	String OR     = " OR ";
	String IN = " IN ";
	String LIKE = " LIKE ";
	String FROM     = " FROM ";
	
	String LESS_THAN = " < ";
	String LESS_THAN_EQUAL = " <= ";
	String GREATER_THAN = " > ";
	String GREATER_THAN_EQUAL = " >= ";
	String EQUAL = " = ";
	String NOT_EQUAL = " != ";
	
	String WHERE     = " WHERE ";
	String HAVING = " HAVING ";
	String SHOW = " SHOW ";
	String TABLE     = " TABLE ";
	String MAX  = " MAX(";
	String VALUES  = " VALUES ";
	
	String INSERT     = " INSERT ";
	String INTO = " INTO ";
	String UPDATE     = " UPDATE ";
	String DELETE = " DELETE ";
	String DROP = " DROP ";
	String SET = " SET ";
	
	String GROUP_BY = " GROUP BY ";
	String ORDER_BY = " ORDER BY ";
	String LIMIT = " LIMIT ";
	String OFFSET = " OFFSET ";
	
	
	
}
