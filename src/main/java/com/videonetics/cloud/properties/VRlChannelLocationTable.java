package com.videonetics.cloud.properties;

public interface VRlChannelLocationTable {
	
	String channelId = "channel_id";
	String locationId = "location_id";
	
	String table_name = "rl_channel_location";

}
