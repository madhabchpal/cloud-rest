package com.videonetics.cloud.properties;

public interface VVideoClipChannelTable {
	
	String id = "id";
	String channelId = "channel_id";
	String clipUrl = "clip_url";
	String junkFlag = "junk_flag";
	String startTimestamp = "start_timestamp";
	String endTimestamp = "end_timestamp";
	String clipSize = "clip_size";
	String dontDelete = "dont_delete";
	String backupState = "backup_state";
	String storagePath = "storage_path";
	String mediaServerId = "media_server_id";
	String table_name = "v_video_clip_";
}
