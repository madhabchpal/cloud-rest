package com.videonetics.cloud.properties;

public interface VRecordingTable {
	
	String id = "id";
	String name = "name";
	String retention_days = "retention_days";
	String motion_recording = "motion_recording";
	String recordingDay_1 = "recording_day_1";
	String recordingDay_2 = "recording_day_2";
	String recordingDay_3 = "recording_day_3";
	String recordingDay_4 = "recording_day_4";
	String recordingDay_5 = "recording_day_5";
	String recordingDay_6 = "recording_day_6";
	String recordingDay_7 = "recording_day_7";
	String recordingDaySpc = "recording_day_spc";
	
	String table_name = "v_recording";

}
