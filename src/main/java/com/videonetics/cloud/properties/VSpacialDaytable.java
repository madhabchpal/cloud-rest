package com.videonetics.cloud.properties;

public interface VSpacialDaytable {
	
	String spacialId = "spc_id";
	String spacialDay = "spc_day";
	String spacialMonth = "spc_month";
	String spacialYear = "spc_year";
	
	String table_name = "v_special_day";

}
