package com.videonetics.cloud.properties;

public interface VRlChannelMapTable {
	
	String channelId = "channel_id";
	String mapId = "map_id";
	String posX = "pos_x";
	String posY = "pos_y";
	
	String table_name = "rl_channel_map";

}
