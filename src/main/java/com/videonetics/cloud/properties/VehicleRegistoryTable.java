package com.videonetics.cloud.properties;

public interface VehicleRegistoryTable {
	
	String id = "id";
	String vehicleNumber = "vehicle_number";
	String ownerName = "owner_name";
	String ownerId = "owner_id";
	String ownerPhone = "owner_phone";
	String addressOne = "address1";
	String addressTwo = "address2";
	String description = "description";
	//TODO: Can add make, model, color of the vehicle
	String vehicleType = "vehicle_type";
	
	String table_name = "v_vehicle_registry";

}
