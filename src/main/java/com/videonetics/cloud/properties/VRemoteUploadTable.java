package com.videonetics.cloud.properties;

public interface VRemoteUploadTable {
	
	String id = "id";
	String title = "title";
	String location = "location";
	String description = "description";
	String upload_url = "upload_url";
	String upload_file_type = "upload_file_type";
	String username = "username";
	String timestamp = "timestamp";
	String snap_status = "snap_status";
	
	String table_name = "v_remote_upload";
	

}
