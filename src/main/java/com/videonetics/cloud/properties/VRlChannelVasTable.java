package com.videonetics.cloud.properties;

public interface VRlChannelVasTable {
	
	String channelId = "channel_id";
	String vasId = "vas_id";
	
	String table_name = "rl_channel_vas";

}
