package com.videonetics.cloud.properties;

public interface VRecycleEventTable {
	
	String id = "id";
	String channelId = "channel_id";
	String eventType = "event_Type";
	String eventMessage = "event_message";
	String action = "action";
	String objectId = "object_id";
	String modifiedObjectId = "modified_object_id";
	String clipReview = "clip_review";
	String acknowledge = "acknowledge";
	String acknowledgeUser = "acknowledge_user";
	String priority = "priority";
	String eventStartTime = "event_starttime";
	String eventEndTime = "event_endtime";
		
	
	String table_name = "v_recycle_event";

}
