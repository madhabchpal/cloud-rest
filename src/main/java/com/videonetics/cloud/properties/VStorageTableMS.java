package com.videonetics.cloud.properties;

public interface VStorageTableMS {
	
	String id = "id";
	String path = "path";
	String type = "type";
	String upload = "upload";
	String mediaServerId = "media_server_id";
	
	String table_name = "v_storage_ms";

}
