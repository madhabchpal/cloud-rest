package com.videonetics.cloud.properties;

public interface VRecycleClipTable {
	
	String id = "id";
	String channelId = "channel_id";
	String clipUrl = "clip_url";
	String junkFlag = "junk_flag";
	String clipStartTimestamp = "clip_start_timestamp";
	
	String table_name = "v_recycle_clip";

}
