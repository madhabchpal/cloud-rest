package com.videonetics.cloud.properties;

public interface VRlChannelRecordingTable {
	
	String channelId = "channel_id";
	String recordingId = "recording_id";
	
	String table_name = "rl_channel_recording";

}
