package com.videonetics.cloud.properties;

public interface VFastDeletableClipTable {
	
	String id = "id";
	String channelId = "channel_id";
	String clipUrl = "clip_url";
	String clipStartTimestamp = "clip_start_timestamp";
	String mediaServerId = "media_server_id";
	
	String table_name = "v_deletable_clip";

}
