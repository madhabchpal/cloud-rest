package com.videonetics.cloud.mongo.collections;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class VRole {
	
	@Id
	private String _id;
	
	@Field("role")
	@Indexed(name = "uk_v_role_role", background = true, unique = true)
	private String role;
	
	@Field("description")
	private String description;
	
	@Field("can_view")
	private Boolean can_view;
	
	@Field("can_edit")
	private Boolean can_edit;
	
	@Field("can_add")
	private Boolean can_add;
	
	@Field("can_delete")
	private Boolean can_delete;
	
	/**
	 * @return the can_view
	 */
	public Boolean getCan_view() {
		return can_view;
	}

	/**
	 * @param can_view the can_view to set
	 */
	public void setCan_view(Boolean can_view) {
		this.can_view = can_view;
	}

	/**
	 * @return the can_edit
	 */
	public Boolean getCan_edit() {
		return can_edit;
	}

	/**
	 * @param can_edit the can_edit to set
	 */
	public void setCan_edit(Boolean can_edit) {
		this.can_edit = can_edit;
	}

	/**
	 * @return the can_add
	 */
	public Boolean getCan_add() {
		return can_add;
	}

	/**
	 * @param can_add the can_add to set
	 */
	public void setCan_add(Boolean can_add) {
		this.can_add = can_add;
	}

	/**
	 * @return the can_delete
	 */
	public Boolean getCan_delete() {
		return can_delete;
	}

	/**
	 * @param can_delete the can_delete to set
	 */
	public void setCan_delete(Boolean can_delete) {
		this.can_delete = can_delete;
	}
	
	/**
	 * @return the _id
	 */
	public String get_id() {
		return _id;
	}

	/**
	 * @param _id the _id to set
	 */
	public void set_id(String _id) {
		this._id = _id;
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
