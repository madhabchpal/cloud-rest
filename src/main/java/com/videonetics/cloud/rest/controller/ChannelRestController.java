package com.videonetics.cloud.rest.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;

import com.videonetics.cloud.vo.ResponseVO;

public interface ChannelRestController {

	ResponseEntity<ResponseVO> getChannels(HttpServletRequest request, HttpServletResponse response);
	
	ResponseEntity<ResponseVO> getChannelById(HttpServletRequest request, HttpServletResponse response, String channelId);
}
