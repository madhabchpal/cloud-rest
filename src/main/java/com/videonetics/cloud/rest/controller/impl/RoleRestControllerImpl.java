package com.videonetics.cloud.rest.controller.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.videonetics.cloud.exception.VReturnValues;
import com.videonetics.cloud.rest.controller.RoleRestController;
import com.videonetics.cloud.service.RoleService;
import com.videonetics.cloud.vo.ResponseVO;

@RestController
@RequestMapping("/rest")
public class RoleRestControllerImpl implements RoleRestController {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(RoleRestControllerImpl.class);

	@Autowired
	private RoleService roleService;

	@Override
	@ResponseBody
	@RequestMapping(value = {"/role"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseVO> getRoles(HttpServletRequest request,
			HttpServletResponse response) {

		LOGGER.info("List Roles");
		
		ResponseVO restResponse = new ResponseVO(VReturnValues.SUCCESS, HttpStatus.OK.value(), "Successfully retrived all roles!", request.getRequestURI(), roleService.getRoles());
		return new ResponseEntity<ResponseVO>(restResponse, HttpStatus.OK);
	}

	@Override
	@ResponseBody
	@RequestMapping(value = {"/role/{roleid}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseVO> getRoleById(HttpServletRequest request,
			HttpServletResponse response, @PathVariable(value="roleid") String roleId) {
		
		ResponseVO restResponse = new ResponseVO(VReturnValues.SUCCESS,
				HttpStatus.OK.value(), "Successfully retrived role id - "
						+ roleId, request.getRequestURI(),
				roleService.getRoles());
		return new ResponseEntity<ResponseVO>(restResponse, HttpStatus.OK);
	}

}
