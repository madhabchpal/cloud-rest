package com.videonetics.cloud.rest.controller.impl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.videonetics.cloud.exception.UnknownResourceException;
import com.videonetics.cloud.rest.controller.DefaultController;

@RestController
//@RequestMapping("/rest")
@RequestMapping(value = {"/**", "/rest/**"})
public class DefaultRestControllerImpl implements DefaultController {

	@Override
	@RequestMapping("/**")
	public void unmappedRequest(HttpServletRequest request) {
		throw new UnknownResourceException(1, HttpStatus.NOT_FOUND, "There is no resource for the requested path");
	}
}
