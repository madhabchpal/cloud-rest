package com.videonetics.cloud.rest.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;

import com.videonetics.cloud.vo.ResponseVO;

public interface RoleRestController {

	ResponseEntity<ResponseVO> getRoles(HttpServletRequest request, HttpServletResponse response);
	
	ResponseEntity<ResponseVO> getRoleById(HttpServletRequest request, HttpServletResponse response, String roleId);
}
