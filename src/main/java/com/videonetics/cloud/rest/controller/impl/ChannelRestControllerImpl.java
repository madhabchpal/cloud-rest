package com.videonetics.cloud.rest.controller.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.videonetics.cloud.exception.VReturnValues;
import com.videonetics.cloud.rest.controller.ChannelRestController;
import com.videonetics.cloud.service.ChannelService;
import com.videonetics.cloud.vo.ResponseVO;

@RestController
@RequestMapping("/rest")
public class ChannelRestControllerImpl implements ChannelRestController {

	private final static Logger LOGGER = LoggerFactory.getLogger(ChannelRestControllerImpl.class);

	@Autowired
	private ChannelService channelService;

	@Override
	@ResponseBody
	@RequestMapping(value = {"/channel"}, method = {RequestMethod.GET, RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseVO> getChannels(HttpServletRequest request,
			HttpServletResponse response) {
		LOGGER.debug("List all channels");
		
		ResponseVO restResponse = new ResponseVO(VReturnValues.SUCCESS,
				HttpStatus.OK.value(), "Successfully retrived all roles!",
				request.getRequestURI(), channelService.getChannels());
		return new ResponseEntity<ResponseVO>(restResponse, HttpStatus.OK);
	}

	@Override
	@ResponseBody
	@RequestMapping(value = {"/channel/{channelid}"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseVO> getChannelById(HttpServletRequest request, HttpServletResponse response,
			@PathVariable(value="channelid") String channelId) {
		LOGGER.debug("Fetch channelid >> {}", channelId);
		
		ResponseVO restResponse = new ResponseVO(VReturnValues.SUCCESS,
				HttpStatus.OK.value(), "Successfully retrived all roles!",
				request.getRequestURI(), channelService.getChannel(channelId));
		return new ResponseEntity<ResponseVO>(restResponse, HttpStatus.OK);
	}
}
