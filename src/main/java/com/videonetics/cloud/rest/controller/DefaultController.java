package com.videonetics.cloud.rest.controller;

import javax.servlet.http.HttpServletRequest;

public interface DefaultController {

	public void unmappedRequest(HttpServletRequest request);
}
