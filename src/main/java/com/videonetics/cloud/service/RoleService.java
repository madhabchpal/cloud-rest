package com.videonetics.cloud.service;

import java.util.List;

import com.videonetics.cloud.mongo.collections.VRole;

public interface RoleService {

	List<VRole> getRoles();

}
