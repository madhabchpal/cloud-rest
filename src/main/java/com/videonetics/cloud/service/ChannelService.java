package com.videonetics.cloud.service;

import java.util.List;

import com.videonetics.model.component.VMediaChannel;

public interface ChannelService {

	List<VMediaChannel> getChannels();

	VMediaChannel getChannel(String channelId);

}
