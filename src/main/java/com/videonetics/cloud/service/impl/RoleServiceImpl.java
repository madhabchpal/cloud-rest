package com.videonetics.cloud.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.videonetics.cloud.dao.RoleDao;
import com.videonetics.cloud.mongo.collections.VRole;
import com.videonetics.cloud.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	private final static Logger LOGGER = LoggerFactory.getLogger(RoleServiceImpl.class);

	@Autowired
	private RoleDao roleDao;
	
	@Override
	public List<VRole> getRoles() {
		LOGGER.debug("Fetching Roles");
		return roleDao.getRoles();
	}

}
