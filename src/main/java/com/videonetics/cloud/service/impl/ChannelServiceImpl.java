package com.videonetics.cloud.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.videonetics.cloud.dao.ChannelDao;
import com.videonetics.cloud.service.ChannelService;
import com.videonetics.model.component.VMediaChannel;

@Service
public class ChannelServiceImpl implements ChannelService {

	private final static Logger LOGGER = LoggerFactory.getLogger(ChannelServiceImpl.class);

	@Autowired
	private ChannelDao channelDao;
	
	@Override
	public List<VMediaChannel> getChannels() {
		LOGGER.debug("Fetching all channels");
		
		return channelDao.getChannels();
	}

	@Override
	public VMediaChannel getChannel(String channelId) {
		LOGGER.debug("Fetching channelid >> {}", channelId);
		
		return channelDao.getChannel(channelId);
	}
}
