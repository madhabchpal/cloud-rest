package com.videonetics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class CloudRestExecutionWorker extends Thread {

	private static final Logger LOGGER = LoggerFactory.getLogger(CloudRestExecutionWorker.class);
	
	@Override
	public void run() {
		LOGGER.info("Shutting down CMSExecutionWorker...");
	}
	
	public static void main(String[] args) {
		@SuppressWarnings({ "resource", "unused" })
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring/spring-bootstrap.xml");
        
		Runtime.getRuntime().addShutdownHook(new CloudRestExecutionWorker());
		LOGGER.info("Started CMSExecutionWorker...");
	}

}
